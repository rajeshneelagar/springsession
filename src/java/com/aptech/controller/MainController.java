/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptech.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ADMINISTRATOR14
 */
@Controller  
@SessionAttributes("username")

public class MainController {
    @RequestMapping("/hello")  
    public ModelAndView helloWorld() {  
        String message = "HELLO SPRING MVC HOW R U";  
        return new ModelAndView("hello", "message", message);  
    }  
    @RequestMapping("/authenticate")  
    public ModelAndView authenticate(@RequestParam("username") String username, @RequestParam("password") String password) {  
        String message = username+" "+password;  
        
        if(true){
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("username", username);
            modelAndView.setViewName("home");
            return modelAndView;

        }
        return new ModelAndView("hello", "message", message);  
    }  
    @RequestMapping("/setting")  
    public ModelAndView setting() {  
        String message = "HELLO SPRING MVC HOW R U";  
        return new ModelAndView("setting", "message", message);  
    } 
    
    @RequestMapping("/logout")  
    public ModelAndView logout() {  
        ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("username", "");
            modelAndView.setViewName("hello");
            return modelAndView; 
    } 
    
}
